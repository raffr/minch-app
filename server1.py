import socket
import _thread
import json

class MainServer:
    def __init__(self,host,port):
        self.host = host
        self.port = port
        self.server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.server.bind((self.host, self.port))
        self.server.listen(10)
        self.id = 1
        print(f"""
=======================
====> Server Started
=======================
<Listen on>
> Host:{self.host}
> Port:{self.port}
""")
        # List client connected
        self.list_client = []
        while True:
            self.client_socket, self.address = self.server.accept()
            print(f"{self.address} is connected")
            # Create new thread for every client socket communication
            _thread.start_new_thread(self.client_conn, (self.client_socket,))

    def client_conn(self,client):
        self.list_client.append(client)
        client.send('{"id":0,"username": "Server","message": "Connected!"}'.encode("utf-8"))
        while True:
            self.recv_msg = client.recv(1024).decode("utf-8")
            self.recv_msg_dict = json.loads(self.recv_msg)
            self.recv_msg_dict.update({"id":self.id})
            self.recv_msg = json.dumps(self.recv_msg_dict)
            self.broadcast(client,self.recv_msg)
            self.id += 1
    # Broadcast message to all client
    def broadcast(self,client,message):
        for all_client in self.list_client:
            if all_client != client:
                try:
                    all_client.send(message.encode("utf-8"))
                except:
                    all_client.close()
            else:
                pass

if __name__ == "__main__":
    address = input("IP Address: ")
    port = int(input("Port: "))
    server1 = MainServer(address,port)
