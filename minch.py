from kivymd.app import MDApp
from kivy.uix.screenmanager import ScreenManager, Screen
from kivymd.uix.navigationdrawer import MDNavigationDrawer
from kivymd.uix.toolbar import MDTopAppBar
from kivymd.uix.boxlayout import MDBoxLayout
from kivymd.uix.label import MDLabel
from kivymd.uix.widget import MDWidget
from kivymd.uix.navigationdrawer import MDNavigationLayout
from kivy.clock import Clock, mainthread
from functools import partial

import json
import threading
import time

from kivy.lang import Builder

from client1 import MainClient

from kivy.core.window import Window
Window.softinput_mode = "below_target"

# Declare navigation drawer
class NavDrawer(MDNavigationDrawer):
    pass
# Declare TopBar 
class TopBar(MDTopAppBar):
    pass
# Declarte bubble message chat box
class BubbleMessage(MDBoxLayout,MDWidget):
    username = ""
    message = ""
    id_msg = None
# Declare Navigation Layout (main layout)
class NavLayout(MDNavigationLayout):
    def __init__(self,**kwargs):
        super(NavLayout,self).__init__(**kwargs)
        self.packet = {}

    def add_bubble(self,dt):
        self.plain_msg = json.loads(MainClient.message)
        if self.plain_msg["id"] == BubbleMessage.id_msg:
            pass
        else:
            BubbleMessage.username = self.plain_msg["username"]
            BubbleMessage.message = self.plain_msg["message"]
            self.ids.scroll_box.add_widget(BubbleMessage())
            BubbleMessage.id_msg = self.plain_msg["id"]

    def connect(self):
        self.packet.update({"username":self.ids.username_input.text})
        self.packet.update({"message":"<joined>"})
        try:
            self.main_client = MainClient(self.ids.host_input.text, int(self.ids.port_input.text), json.dumps(self.packet))
            self.ids.status_connection.text = "Status: Connected!"
            Clock.schedule_interval(self.add_bubble,0.1)
        except ConnectionRefusedError:
            self.ids.status_connection.text = "Status: Connection Failed"
        except:
            pass
        else:
            self.ids.status_connection.text = "Status: Connected!"
    def send(self):
        try:
            # send message to server
            self.str_msg = str(self.ids.msg_box.text)
            self.packet.update({"message":self.str_msg})
            self.main_client.send_msg(message=json.dumps(self.packet))

            # update the bubble message
            BubbleMessage.username = self.packet["username"]
            BubbleMessage.message = self.str_msg
            self.ids.scroll_box.add_widget(BubbleMessage(md_bg_color="#D1C4E9"))

            # clear the message box
            self.ids.msg_box.text = ""
        except:
            pass

class MainApp(MDApp):

    # Declare variable for chat username and chat message
    username = ""
    message = None

    # For updating and spawn new message
    def add_bubble(self,username,message):
        self.ids.scroll_view.add_widget(BubbleMessage())

    def build(self):
        self.theme_cls.primary_palette = "DeepPurple"
        self.theme_cls.primary_hue = "300"
        self.theme_cls.theme_style = "Light"
        self.main_design = Builder.load_file("minch-design.kv")
        return self.main_design

if __name__ == "__main__":
    MainApp().run()
