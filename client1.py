import socket

#import tkinter

import threading

class MainClient:
    message = None
    def __init__(self,host,port,message):
        self.host = host
        self.port = port
        self.client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.client.connect((self.host, self.port))
        ## Thread for receiving image in infinite loop
        self.thread1 = threading.Thread(target=self.rcv_msg,daemon=True,).start()
        ## Thread for send msg
        self.thread2 = threading.Thread(target=self.send_msg,daemon=True,args=(message,)).start()

    def rcv_msg(self):
        while True:
            self.recv_msg_str = self.client.recv(1024).decode("utf-8")
            MainClient.message = self.recv_msg_str
    def send_msg(self,message):
        self.client.send(message.encode("utf-8"))
